package com.kBrains.etl.user;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.kBrains.etl.user.UserMaria.UserMariaBuilder;

/**
 * @author Alfredo Ferreira
 */

public class UserMapper implements RowMapper<UserPostgres> { 
	@Override
	public UserPostgres mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserPostgres user = new UserPostgres(
				rs.getInt("id"), 
				rs.getString("username"), 
				rs.getString("email"), rs.getString("full_name"),
				rs.getDate("created_date"),
				rs.getTime("created_time"));
        		
		return user;
	}
}


