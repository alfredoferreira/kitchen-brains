package com.kBrains.etl.user;

/**
 * @author Alfredo Ferreira
 */

public class User {
	private Integer id;
	private String username;
	private String email;
	
	/**
	 * Constructor
	 */
	public User() {
    }
	
	/**
	 * Constructor
	 * 
	 * @param id
	 * @param username
	 * @param email
	 */
	public User(Integer id, String username, String email) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
	}

	/**
	 * Gets id
	 * @return Integer id
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Sets id
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * Gets user name
	 * @return String user name
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * Sets user name
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	/**
	 * Gets email
	 * @return String email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Sets email
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
}
