package com.kBrains.etl.user;

import java.sql.Date;
import java.sql.Time;

/**
 * @author Alfredo Ferreira
 */

public class UserPostgres extends User{
	private String fullName;
	private Date createDate;
	private Time createTime;
	
	
	/**
	 * Constructor
	 */
	public UserPostgres() {
		super();
	}

	/**
	 * Constructor
	 * 
	 * @param id
	 * @param username
	 * @param email
	 * @param fullName
	 * @param createdDate
	 * @param createdTime
	 */
	public UserPostgres(Integer id, String username, String email, 
			String fullName, Date createdDate, Time createdTime) {
		super(id, username, email);

		this.fullName = fullName;
		this.createDate = createdDate;
		this.createTime = createdTime;
	}

	/**
	 * Gets full name
	 * @return String full name
	 */
	public String getFullName() {
		return fullName;
	}
	
	/**
	 * Sets full name
	 * @param fullName
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	/**
	 * Gets created date
	 * @return Date created date
	 */
	public Date getCreateDate() {
		return createDate;
	}
	
	/**
	 * Sets created Date
	 * @param createDate
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Gets created time
	 * @return Time created time
	 */
	public Time getCreateTime() {
		return createTime;
	}
	
	/**
	 * Sets created time
	 * @param createTime
	 */
	public void setCreateTime(Time createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "UserPostgres [id=" + this.getId() + ", username=" + this.getUsername() + ", email=" + this.getEmail()
				+ "fullName=" + fullName + ", createDate=" + createDate + ", createTime=" + createTime + "]";
	}
	
}
