package com.kBrains.etl.user;

import java.sql.Timestamp;


/**
 * @author Alfredo Ferreira
 */

public class UserMaria extends User{
	private String firstName;
	private String lastName;
	private Timestamp created;
	
	/**
	 * Constructor
	 * 
	 * @param builder
	 */
	public UserMaria(UserMariaBuilder builder) {
		super(builder.id, builder.username, builder.email);
		this.firstName = builder.firstName;
		this.lastName = builder.lastName;
		this.created = builder.created;
	}
		
	
	/**
	 * Constructor
	 */
	public UserMaria() {
		super();
	}
	
	/**
	 * Constructor
	 * 
	 * @param id
	 * @param username
	 * @param email
	 * @param firstName
	 * @param lastName
	 * @param created
	 */
	public UserMaria(Integer id, String username, String email, String firstName, String lastName, Timestamp created) {
		super(id, username, email);
		
		this.firstName = firstName;
		this.lastName  = lastName;
		this.created   = created;
	}
	
	/**
	 * Gets first name
	 * 
	 * @return String first name
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * Sets first name
	 * 
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * Gets last name
	 * 
	 * @return String last name
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * Sets last name
	 * 
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * Gets created 
	 * 
	 * @return created
	 */
	public Timestamp getCreated() {
		return created;
	}
	
	/**
	 * Sets created
	 * 
	 * @param created
	 */
	public void setCreated(Timestamp created) {
		this.created = created;
	}

	@Override
	public String toString() {
		return "UserMaria [id=" + this.getId() + ", username=" + this.getUsername() + ", email=" + this.getEmail() +
				", firstName=" + firstName + ", lastName=" + lastName + ", created=" + created + "]";
	}
	
	// Adding Builder Pattern
	public static class UserMariaBuilder {
		private Integer id;
		private String username;
		private String email;
		private String firstName;
		private String lastName;
		private Timestamp created;
	    
		/**
		 * Constructor
		 * 
		 * @param username
		 * @param email
		 * @param firstName
		 */
	    public UserMariaBuilder(Integer id, String username, String email, String firstName) {
	    	this.id = id;
            this.username = username;
            this.email = email;
            this.firstName = firstName;
        }
	    
	    /**
	     * Sets last name
	     * 
	     * @param lastName
	     * @return UserMariaBuilder
	     */
	    public UserMariaBuilder withOptionalLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }
	    
	    /**
	     * Sets created 
	     * 
	     * @param created
	     * @return UserMariaBuilder
	     */
	    public UserMariaBuilder withOptionalCreated(Timestamp created) {
            this.created = created;
            return this;
        }
	    
	    /**
	     * Builds and returns UserMaria
	     * 
	     * @return UserMaria
	     */
	    public UserMaria build() {
            isValidateProductData();
            return new UserMaria(this);
        }
	    
	    /**
	     * Validate data
	     * @return boolean
	     */
	    private boolean isValidateProductData() {
            //Do some basic validations to check
            return true;
        }
 
	}
}
