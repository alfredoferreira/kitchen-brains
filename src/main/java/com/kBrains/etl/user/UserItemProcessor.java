package com.kBrains.etl.user;


import java.util.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.util.StringUtils;

import com.kBrains.etl.user.UserMaria.UserMariaBuilder;

/**
 * @author Alfredo Ferreira
 */

public class UserItemProcessor implements ItemProcessor<UserPostgres, UserMaria> {
	
	private static final String DOMAIN_NAME_FROM = "fastinc.com";
	private static final String DOMAIN_NAME_TO   = "kitchenbrains.com";
	

    private static final Logger log = LoggerFactory.getLogger(UserItemProcessor.class);

    @Override
    public UserMaria process(final UserPostgres user) throws Exception {
    	// Field "email" must be change the email domain from "fastinc.com" to "kitchenbrains.com"
    	final String email = transformEmail(user.getEmail());
    	
    	// Field "full_name" must split a single string representing 
    	// the full name into two fields for first name and last name using the blank space as a delimiter.
    	String fullName = user.getFullName(); 
        final String firstName = StringUtils.capitalize(getFirstNameFromFullName(fullName));
        final String lastName = StringUtils.capitalize(getLastNameFromFullName(fullName));
    	
        
        // Fields "created_date" and "created_time" in the source must be 
        // combined into a single timestamp field in the target.
        Timestamp created = convertStringToTimestamp(user.getCreateDate().toString() +" "+ user.getCreateTime().toString());

        final UserMaria transformedPerson = new UserMariaBuilder(user.getId(), user.getUsername(), email, firstName)
        		.withOptionalLastName(lastName)
        		.withOptionalCreated(created)
        		.build();

        log.info("Converting (" + user + ") into (" + transformedPerson + ")");

        return transformedPerson;
    }
    
    /**
     * Transform email based on business rules
     * 
     * @param email
     * @return String email
     */
    private static String transformEmail(String email) {
    	return email.replace(DOMAIN_NAME_FROM, DOMAIN_NAME_TO);
    }
    
    
    /**
     * Extract first name from full name
     * 
     * @param fullName
     * @return String first name
     */
    private static String getFirstNameFromFullName(String fullName) {
    	return (fullName.indexOf(" ") == -1 ? fullName : fullName.substring(0, fullName.indexOf(" ")));
    }
    
    /**
     * Extract last name from full name
     * 
     * @param fullName
     * @return String last name
     */
    private static String getLastNameFromFullName(String fullName) {
    	return (fullName.indexOf(" ") == -1 ? "" : fullName.substring(fullName.indexOf(" ")+1));
    }
    
    /**
     * Converts string date to sql timestamp
     * 
     * @param strDate
     * @return Timestamp
     */
    private static Timestamp convertStringToTimestamp(String strDate) {
    	try {
        	DateFormat formatter;
        	formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	Date date = (Date) formatter.parse(strDate);
        	java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());
        	return timeStampDate;
        } catch (ParseException e) {
          System.out.println("Exception :" + e);
          return null;
        }
      }

}
