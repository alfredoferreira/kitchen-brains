package com.kBrains.etl.configuration;

import javax.annotation.PostConstruct;

import org.flywaydb.core.Flyway;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FlywaySlaveInitializer {

   @PostConstruct
    public void migrateFlyway() {
        Flyway flyway = Flyway.configure().dataSource("jdbc:postgresql://localhost:5432/", "postgres", "root")
        		.locations("db/postgres")
        		.load();
        flyway.migrate();
    }

}
