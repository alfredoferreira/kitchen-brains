package com.kBrains.etl.configuration;

import java.net.MalformedURLException;
import java.sql.ResultSet;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import com.kBrains.etl.user.UserItemProcessor;
import com.kBrains.etl.user.UserMapper;
import com.kBrains.etl.user.UserMaria;
import com.kBrains.etl.user.UserPostgres;
import com.zaxxer.hikari.HikariDataSource;


/**
 * @author Alfredo Ferreira
 */

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {
	
	@Autowired
    private JobBuilderFactory jobBuilderFactory;
 
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    
    
    @Bean
    @Primary
    @ConfigurationProperties("app.datasource.first")
    public DataSourceProperties firstDataSourceProperties() {
    	return new DataSourceProperties();
    }

    
    @Bean(name = "mariaDb")
    @Primary
    @ConfigurationProperties("app.datasource.first.configuration")
    public HikariDataSource mariaDataSource() {
    	return firstDataSourceProperties().initializeDataSourceBuilder()
    			.type(HikariDataSource.class).build();
    }
    
    
    @Bean
    @ConfigurationProperties("app.datasource.second")
    public DataSourceProperties secondDataSourceProperties() {
    	return new DataSourceProperties();
    }

    @Bean(name = "postgresDb")
    @ConfigurationProperties("app.datasource.second.configuration")
    public HikariDataSource postgresDataSource() {
    	return secondDataSourceProperties().initializeDataSourceBuilder()
    			.type(HikariDataSource.class).build();
    }

    
	@Bean
	public ItemReader<UserPostgres> reader() {
	    JdbcCursorItemReader<UserPostgres> itemReader = new JdbcCursorItemReader<>();
	    itemReader.setDataSource(postgresDataSource());
	    itemReader.setSql("SELECT id, username, email, full_name, created_date, created_time from user_principal");
	    itemReader.setFetchSize(10);
	    itemReader.setRowMapper(new UserMapper());
	    return itemReader;
	}

    @Bean
    public UserItemProcessor processor() {
        return new UserItemProcessor();
    }

    @Bean
    public JdbcBatchItemWriter<UserMaria> writer() {
        return new JdbcBatchItemWriterBuilder<UserMaria>()
            .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
            .sql("INSERT INTO user_principal (id, username, email, first_name, last_name, created) VALUES (:id, :username, :email, :firstName, :lastName, :created)")
            .dataSource(mariaDataSource())
            .build();
    }
    
    
    @Bean
    public Job importUserJob(JobCompletionNotificationListener listener, Step step1) {
        return jobBuilderFactory.get("importUserJob")
            .incrementer(new RunIdIncrementer())
            .listener(listener)
            .flow(step1)
            .end()
            .build();
    }

    @Bean
    public Step step1(JdbcBatchItemWriter<UserMaria> writer) {
        return stepBuilderFactory.get("step1")
            .<UserPostgres, UserMaria> chunk(10)
            .reader(reader())
            .processor(processor())
            .writer(writer)
            .build();
    }
}
