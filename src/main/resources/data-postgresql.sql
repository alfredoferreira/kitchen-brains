INSERT INTO user_principal(id, username, email, full_name, created_date, created_time)
VALUES (1, 'admin', 'admin@fastinc.com', 'Admin', '2019-01-02', '10:23:54'),
	   (2, 'john.doe', 'john.doe@fastinc.com', 'John Doe', '2019-01-03', '11:34:55'),
	   (3, 'jane.doe', 'jane.doe@fastinc.com', 'jane doe', '2019-01-04', '12:45:56');
