CREATE TABLE IF NOT EXISTS user_principal ( 
   id SERIAL PRIMARY KEY, 
   username VARCHAR(100) NOT NULL, 
   email VARCHAR(100) NOT NULL, 
   full_name VARCHAR(100) NOT NULL, 
   created_date DATE NULL,
   created_time TIME NULL
);

ALTER TABLE user_principal OWNER TO postgres;